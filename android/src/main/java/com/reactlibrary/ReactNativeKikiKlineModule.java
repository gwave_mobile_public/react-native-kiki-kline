// ReactNativeKikiKlineModule.java

package com.reactlibrary;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.uimanager.events.RCTEventEmitter;
import com.kikitrade.kikikline.config.CryptoChartGlobalConfig;
import com.kikitrade.kikikline.view.ChartViewDataContainerView;

import java.util.Map;

public class ReactNativeKikiKlineModule extends SimpleViewManager<ChartViewDataContainerView> {

    private final ReactApplicationContext reactContext;

    public static final String CHART_STORAGE_FILE_NAME = "chart_data";

    public ReactNativeKikiKlineModule(ReactApplicationContext reactContext) {
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "ReactNativeKikiKline";
    }

    @NonNull
    @Override
    protected ChartViewDataContainerView createViewInstance(@NonNull final ThemedReactContext context) {

       final ChartViewDataContainerView mChartView = new ChartViewDataContainerView(reactContext);

        mChartView.setChartViewInterface(new ChartViewDataContainerView.OnChartViewLoadingInterface() {
            @Override
            public void showLoading(boolean loading) {
                // 这里传了个空的 event 对象，使用时可以在 event 中加入要传输的数据
                WritableMap event = Arguments.createMap();
                event.putBoolean("loading", loading);
                context.getJSModule(RCTEventEmitter.class).receiveEvent(
                        mChartView.getId(),
                        "onNativeClick", // 与下面注册的要发送的事件名称必须相同
                        event);
            }
        });
        return mChartView;
    }

    @ReactProp(name = "config")
    public void setConfig(ChartViewDataContainerView chartView, @Nullable ReadableMap config) {
        boolean isConfig = config.getBoolean("isConfig");
        if (!isConfig) {
            return;
        }
        chartView.setSPFileName(CHART_STORAGE_FILE_NAME);
        chartView.initView(new CryptoChartGlobalConfig(config.getString("locale"),
                config.getString("timeType"), config.getString("coinPrecision"),
                config.getString("environment"), config.getString("coinCode"),
                config.getString("jwtToken"), "",config.getString("tradeVolumePrecision")));
    }

    public Map getExportedCustomBubblingEventTypeConstants() {
        return MapBuilder.builder()
                .put(
                        "onNativeClick",
                        MapBuilder.of("phasedRegistrationNames",
                                MapBuilder.of("bubbled", "onReactClick"))).build();
    }
}
