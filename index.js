// main index.js

import {NativeModules, requireNativeComponent} from 'react-native';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

const Chart = requireNativeComponent('ReactNativeKikiKline')

export default class KKCryptoChart extends Component {
    // eslint-disable-next-line flowtype/require-return-type
    render (): Element<*> {
        return <Chart {...this.props}/>;
    }
}
KKCryptoChart.propTypes = {
    config: PropTypes.object,
    onReactClick: PropTypes.func
};
